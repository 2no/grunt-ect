'use strict';

module.exports = function(grunt)
{
  grunt.initConfig({
      jshint: {
          all: [
              'Gruntfile.js'
            , 'tasks/*.js'
            , '<%= nodeunit.tests %>'
          ]
        , options: {
            jshintrc: '.jshintrc'
          }
      }

      // Before generating any new files, remove any previously-created files.
    , clean: {
        tests: ['tmp']
      }

    , ect: {
          renderCompatibility: {
              options: {
                root: 'test/fixtures/ect'
              }
            , src:  'page'
            , dest: 'tmp/compatibility/page.html'
            , variables: {
                  title: 'Hello, world!'
                , id: 'main'
                , links: [
                      { name: 'Google',   url: 'http://google.com/'   }
                    , { name: 'Facebook', url: 'http://facebook.com/' }
                    , { name: 'Twitter',  url: 'http://twitter.com/'  }
                  ]
                , upperHelper: function (string) {
                    return string.toUpperCase();
                  }
              }
          }
        , renderDefault: {
            files: {
                // 1:1 render
                'tmp/default/ect.txt': 'test/fixtures/ect1.ect'
                // concat then render into single file
              , 'tmp/default/concat.txt': [
                    'test/fixtures/ect1.ect'
                  , 'test/fixtures/ect2.ect'
                ]
            }
          }
        , renderCustomOptions1: {
              options: {
                root: 'test/fixtures'
              }
            , files: {
                'tmp/customOptions1/concat.txt': '*.ect'
              }
          }
        , renderCustomOptions2: {
              options: {
                variables: {
                    title: 'Hello, world!'
                  , id: 'main'
                  , links: [
                        { name: 'Google',   url: 'http://google.com/'   }
                      , { name: 'Facebook', url: 'http://facebook.com/' }
                      , { name: 'Twitter',  url: 'http://twitter.com/'  }
                    ]
                  , upperHelper: function (string) {
                      return string.toUpperCase();
                    }
                }
              }
            , files: {
                'tmp/customOptions2/page.html': 'test/fixtures/ect/page'
              }
          }
          // use JavaScript object as root.
        , renderCustomOptions3: {
              options: {
                  root: {
                      layout: '<html><head><title>[%- @title %]</title></head><body>[% content %]</body></html>'
                    , page: '[% extend "layout" %]<p>Page content</p>'
                  }
                , open: '[%'
                , close: '%]'
                , variables: {
                    title: 'Hello, world!'
                  }
              }
            , src:  'page'
            , dest: 'tmp/customOptions3/page.html'
          }
        , renderEach: {
              options: {
                  variables: {
                    title: 'Hello, world!'
                  }
                , ext: '.html'
              }
            , expand: true
            , flatten: true
            , cwd: 'test/fixtures/view'
            , src: '*.html'
            , dest: 'tmp/each'
            , ext: '.html'
          }
      }

      // Unit tests.
    , nodeunit: {
        tests: ['test/*_test.js']
      }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');

  // Whenever the "test" task is run, first clean the "tmp" dir, then run this
  // plugin's task(s), then test the result.
  grunt.registerTask('test', ['clean', 'ect', 'nodeunit']);

  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'test']);

};
