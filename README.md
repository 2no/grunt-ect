# grunt-ect

## Getting Started

install via npm

```shell
npm install grunt-ect
```

and in your Gruntfile.js file:

```shell
grunt.loadNpmTasks('grunt-ect');
```

### Prerelease version

install via bitbucket repository

```shell
npm install git+https://2no@bitbucket.org/2no/grunt-ect.git#develop
```

## Usage Examples

```js
ect: {
  render: {
    files: {
      'path/to/result.html': 'path/to/template.ect', // 1:1 render
      'path/to/another.html': ['path/to/templates/*.ect', 'path/to/more/*.ect'] // render and concat into single file
    },
  },
  renderCustomOptions1: {
    options: {
      root: 'path/to',
      variables: {
        title: 'Hello, world!',
        id: 'main',
        links: [
          { name: 'Google',   url: 'http://google.com/'   },
          { name: 'Facebook', url: 'http://facebook.com/' },
          { name: 'Twitter',  url: 'http://twitter.com/'  },
        ],
        upperHelper: function (string) {
          return string.toUpperCase();
        },
      },
    },
    files: {
      'path/to/result.html': 'template.ect',
    },
  },
  renderCustomOptions2: {
    options: {
      root: {
        layout: '<html><head><title>[%- @title %]</title></head><body>[% content %]</body></html>',
        page: '[% extend "layout" %]<p>Page content</p>',
      },
      open: '[%',
      close: '%]',
      variables: {
        title: 'Hello, world!',
      },
    },
    src:  'page',
    dest: 'path/to/result.html',
  },
  renderEach: {
    options: {
      variables: {
        title: 'Hello, world!',
      },
      ext: '.html',
    },
    expand: true,
    flatten: true,
    cwd: 'path/to',
    src: '*.html',
    dest: 'path/to',
    ext: '.html',
  },

  // v0.0.1
  renderCompatibility: {
    options: {
      root: 'path/to',
    },
    src:  'template',
    dest: 'path/to/result.html',
    variables: {
      title: 'Hello, world!',
      id: 'main',
      links: [
        { name: 'Google',   url: 'http://google.com/'   },
        { name: 'Facebook', url: 'http://facebook.com/' },
        { name: 'Twitter',  url: 'http://twitter.com/'  },
      ],
      upperHelper: function (string) {
        return string.toUpperCase();
      },
    },
  },
}
```

run with:

```shell
grunt ect
```

## Options

### Original Renderer Options

* `root` — Templates root folder or JavaScript object containing templates
* `ext` — Extension of templates, defaulting to `''` (not used for JavaScript objects as root)
* `open` — Open tag, defaulting to `<%`
* `close` — Closing tag, defaulting to `%>`

### Added grunt-ect Options

* `variables` — Data passed to template
* `separator` — Concatenated files will be joined on this string, defaulting to `linefeed`

## Release History

* 2013-05-28 v0.1.2 Fixed bug with each file export.
* 2013-05-04 v0.1.1 Bug fix. Changed the file deployment method and pattern matching method.
* 2013-04-23 v0.1.0 Support wildcard pattern and JavaScript object as root. Extension of the setting method.
* 2013-04-05 v0.0.1 first version
