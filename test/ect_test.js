'use strict';

var grunt = require('grunt');

function readFile(file)
{
  var contents = grunt.file.read(file);

  if (process.platform === 'win32') {
    contents = contents.replace(/\r\n/g, '\n');
  }

  return contents;
}

function assertFileEquality(test, pathToActual, pathToExpected, message)
{
  test.equal(readFile(pathToExpected), readFile(pathToActual), message);
}

exports.ect = {
    renderCompatibility: function(test)
    {
      test.expect(1);

      assertFileEquality(test,
        'tmp/compatibility/page.html',
        'test/expected/compatibility/page.html',
        'Should render ect to single file.');

      test.done();
    }
  , renderDefault: function(test)
    {
      test.expect(2);
      
      assertFileEquality(test,
        'tmp/default/ect.txt',
        'test/expected/default/ect.txt',
        'Should render ect to single file.');

      assertFileEquality(test,
        'tmp/default/concat.txt',
        'test/expected/default/concat.txt',
        'Should render ect files to concatenate them into a single file.');

      test.done();
    }
  , renderCustomOptions1: function(test)
    {
      test.expect(1);

      assertFileEquality(test,
        'tmp/customOptions1/concat.txt',
        'test/expected/customOptions1/concat.txt',
        'Should render ect files to concatenate them into a single file.');

      test.done();
    }
  , renderCustomOptions2: function(test)
    {
      test.expect(1);

      assertFileEquality(test,
        'tmp/customOptions2/page.html',
        'test/expected/customOptions2/page.html',
        'Should render ect to single file.');

      test.done();
    }
  , renderCustomOptions3: function(test)
    {
      test.expect(1);

      assertFileEquality(test,
        'tmp/customOptions3/page.html',
        'test/expected/customOptions3/page.html',
        'Should render ect to single file.');

      test.done();
    }
  , renderEach: function(test)
    {
      test.expect(5);

      assertFileEquality(test,
        'tmp/each/footer.html',
        'test/expected/each/footer.html',
        'Should render ect to single file.');

      assertFileEquality(test,
        'tmp/each/header.html',
        'test/expected/each/header.html',
        'Should render ect to single file.');

      assertFileEquality(test,
        'tmp/each/layout.html',
        'test/expected/each/layout.html',
        'Should render ect to single file.');

      assertFileEquality(test,
        'tmp/each/page.html',
        'test/expected/each/page.html',
        'Should render ect to single file.');

      assertFileEquality(test,
        'tmp/each/sublayout.html',
        'test/expected/each/sublayout.html',
        'Should render ect to single file.');

      test.done();
    }
};
