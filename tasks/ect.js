/*
 * grunt-ect
 * https://bitbucket.org/2no/grunt-ect
 *
 * Copyright (c) 2013 2no
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt)
{
  var ECT  = require('ect')
    , path = require('path')
    , _ = grunt.util._
    ;

  grunt.registerMultiTask('ect', 'generates an file from a ect template', function() {
    var options = this.options({
            variables: this.data.variables || {}
          , separator: grunt.util.linefeed
        })
      , done = this.async()
      ;

    if (typeof options.watch !== 'undefined') {
      delete options['watch'];
    }

    grunt.verbose.writeflags(options, 'Options');

    this.files.forEach(function(f) {
      try {
        var validFiles = getSrc(f, options);
        writeFile(f.dest, concatOutput(validFiles, options));
      }
      catch (e) {
        grunt.log.error(e);
        done(false);
      }
    });

    done(true);
  });

  function getSrc(file, options)
  {
    var src  = file.src
      , orig = _.clone(file.orig)
      , root = options.root
      ;

    switch (grunt.util.kindOf(root)) {
      case 'object':
        src = Object.keys(root).filter(function(key) {
          var i = orig.src.length;
          while (i--) {
            if (grunt.file.minimatch(key, orig.src[i])) {
              return true;
            }
          }
          return false;
        });
        break;

      case 'string':
        if (typeof orig.cwd === 'undefined') {
          orig.cwd = root;
          src = grunt.task.normalizeMultiTaskFiles(orig).map(function(v) {
            return v.src.map(function(v) {
              return !grunt.file.doesPathContain(root, v) ?
                path.join(root, path.sep, v) : v;
            });
          });
          src = Array.prototype.concat.apply([], src);
        }
        /* falls through */
      default:
        src = removeInvalidFiles(src);
        break;
    }

    return src;
  }

  function removeInvalidFiles(files)
  {
    return files.filter(function(filepath) {
      if (!grunt.file.exists(filepath)) {
        grunt.log.warn('Source file "' + filepath + '" not found."');
        return false;
      }
      else {
        return true;
      }
    });
  }

  function concatOutput(files, options)
  {
    return files.map(function(src) {
      return render(src, options);
    }).join(grunt.util.normalizelf(options.separator));
  }

  function render(src, options)
  {
    var renderer, root;

    options = _.clone(options);
    switch (grunt.util.kindOf(options.root)) {
      case 'object':
        break;

      case 'string':
        root = path.join(options.root, path.sep);
        if (grunt.file.doesPathContain(root, src)) {
          src = path.relative(root, src);
        }
        break;

      default:
        options.root = path.dirname(src);
        src = path.basename(src);
        break;
    }

    renderer = new ECT(options);
    return renderer.render(src, options.variables);
  }

  function warnOnEmptyFile(path)
  {
    grunt.log.warn('Destination (' + path + ') not written because rendered files were empty.');
  }

  function writeFile(path, output)
  {
    if (output.length < 1) {
      warnOnEmptyFile(path);
    }
    else {
      grunt.file.write(path, output);
      grunt.log.writeln('File ' + path + ' created.');
    }
  }
};
